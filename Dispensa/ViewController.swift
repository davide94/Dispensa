//  
//  ViewController.swift
//  Dispensa
//
//  Created by Davide Rossetto on 19/03/15.
//  Copyright (c) 2015 Davide Rossetto. All rights reserved.
//

import UIKit
import QuartzCore

class ViewController: UIViewController, UIScrollViewDelegate {

    let items = [   ["title": "Girelle di mortadella", "text": ["Fesa di tacchino a fette", "Mortadella", "Olio extravergine d'oliva", "Panna", "Pepe nero", "Pistacchi", "Rape", "Sale", "Vino bianco"]],
        ["title": "Pollo alle mandorle", "text": ["Petto di pollo", "Mandorle", "Cipolle", "Zenzero", "Fecola di patate", "Salsa di soia"]],
                    ["title": "Abbacchio A Scottadito", "text": ["Abbacchio a fette", "Olio", "Sale", "Pepe", "Rosmarino", "Limone"]],
                    ["title": "Aragosta allo Champagne", "text": ["Aragosta", "Funghi", "Panna", "Tartufo", "Prezzemolo","Pepe","Champagne"]],
                    ["title": "Caesar salad", "text": ["Lattuga", "Pane","Parmigiano reggiano","Aglio","Worcestershire sauce","Uova","Sale","Aceto","Limone","Pepe","Olio d'oliva"]]]
    
    let myRed:UIColor = UIColor(red: 232/255, green: 62/255, blue: 81/255, alpha: 1)
    var width:CGFloat = 0.0
    var background:UIImageView = UIImageView();
    var topBar:UIView = UIView()
    
    //inizio
    let SPAN:CGFloat = 100
    let PADDING_TOP:CGFloat = CGFloat(210.0)
    
    var open = false
    
    let scroll:UIScrollView = UIScrollView()
    //fine
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        width = self.view.frame.size.width
        background.frame = self.view.frame
        background.contentMode = UIViewContentMode.ScaleAspectFill
        //background.contentMode = UIViewContentMode.TopLeft
        //background.image = UIImage(named: "freego-24")
        //background.image = UIImage(named: "sfondo-2-27")
        background.image = img("background")
        
        self.view.addSubview(background)
        
        topBar.frame = CGRectMake(0.0, 0.0, width, 60.0)
        //topBar.backgroundColor = myRed
        
        var settingsButton:UIButton = UIButton()
        settingsButton.frame = CGRectMake(10.0, 20.0, 40.0, 40.0)
        settingsButton.setImage(UIImage(named: "settingsIcon2w"), forState: .Normal)
        //settingsButton.addTarget(self, action: "Action:", forControlEvents: UIControlEvents.TouchUpInside)
        //topBar.addSubview(settingsButton)
        
        var scanButton:UIButton = UIButton()
        scanButton.frame = CGRectMake(self.view.frame.width-50.0, 20.0, 40.0, 40.0)
        scanButton.setImage(UIImage(named: "barcodeIcon"), forState: .Normal)
        scanButton.addTarget(self, action: "scan:", forControlEvents: UIControlEvents.TouchUpInside)
        topBar.addSubview(scanButton)
        
        var pickerView:UIView = UIView()
        pickerView.tag = -1
        pickerView.frame = CGRectMake(0.0, 20.0, width, 190)
        
        var picker:UIScrollView = UIScrollView()
        
        picker.delegate = self
        picker.frame = CGRectMake(0.0, 40.0, width, 150)
        picker.contentSize = CGSizeMake(3*width, 150.0)
        picker.pagingEnabled = true
        picker.showsHorizontalScrollIndicator = false
        picker.showsVerticalScrollIndicator = false
        
        var searchField:UITextField = UITextField(frame: CGRectMake(70.0, 7.0, width-140.0, 26.0))
        searchField.backgroundColor = UIColor(white: 1.0, alpha: 0.3)
        searchField.textColor = UIColor.whiteColor()
        searchField.layer.cornerRadius = 13
        searchField.layer.borderColor = UIColor(white: 1.0, alpha: 0.5).CGColor
        searchField.layer.borderWidth = 0.5
        searchField.clipsToBounds = true
        pickerView.addSubview(searchField)
        
        var b1:UIButton = UIButton()
        b1.frame = CGRectMake((width/2)-50.0, 10.0, 100.0, 100.0)
        //b1.setImage(UIImage(named: "icon-1-w"), forState: .Normal)
        b1.setImage(img("ico-1-w"), forState: .Normal)
        //b1.addTarget(self, action: "Action:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var b2:UIButton = UIButton()
        b2.frame = CGRectMake((3*width/2)-50.0, 10.0, 100.0, 100.0)
        //b2.setImage(UIImage(named: "icon-2-w"), forState: .Normal)
        b2.setImage(img("ico-2-w"), forState: .Normal)
        //b2.addTarget(self, action: "Action:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var b3:UIButton = UIButton()
        b3.frame = CGRectMake((5*width/2)-50.0, 10.0, 100.0, 100.0)
        b3.setImage(img("ico-3-w"), forState: .Normal)
        //b3.addTarget(self, action: "Action:", forControlEvents: UIControlEvents.TouchUpInside)
        picker.addSubview(b1)
        picker.addSubview(b2)
        picker.addSubview(b3)
        pickerView.addSubview(picker)
        
        var arrowLeft:UIButton = UIButton()
        arrowLeft.frame = CGRectMake(20.0, 85.0, 12.2, 40.0)
        arrowLeft.setImage(UIImage(named: "arrowLeft"), forState: .Normal)
        //arrowLeft.addTarget(self, action: "Action:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var arrowRight:UIButton = UIButton()
        arrowRight.frame = CGRectMake(width-32.2, 85.0, 12.2, 40.0)
        arrowRight.setImage(UIImage(named: "arrowRight"), forState: .Normal)
        //arrowRight.addTarget(self, action: "Action:", forControlEvents: UIControlEvents.TouchUpInside)
        pickerView.addSubview(arrowLeft)
        pickerView.addSubview(arrowRight)
        
        var label:UILabel = UILabel()
        label.frame = CGRectMake((width/2)-100.0, 160.0, 200.0, 20.0)
        label.text = "Ricette"
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Center;
        
        pickerView.addSubview(label)
        
        //inizio
        /*let iv:UIImageView = UIImageView(frame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height))
        iv.image = UIImage(named: "Background")
        self.view.addSubview(iv)*/
        
        scroll.frame = self.view.frame
        self.view.addSubview(scroll)
        scroll.delegate = self
        scroll.showsVerticalScrollIndicator = false
        scroll.backgroundColor = UIColor.clearColor()//myRed
        scroll.addSubview(pickerView)
        
        //fine
        fillPassView(items)
        
        self.view.addSubview(topBar)

    }
    
    //MARK: Picker
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if scrollView != scroll{
            /*var index = Int(scrollView.contentOffset.x/width)
            switch Int(scrollView.contentOffset.x/width) {
            case 1:
                fillPassView(items2)
            default:
                fillPassView(items)
            }*/
        }
        //tableView.reloadData()
    }
    
    //MARK: Passbook
    
    func fillPassView(items:NSArray) {
        for sub in self.scroll.subviews as [UIView] {
            if sub.tag != -1 {
                sub.removeFromSuperview()
            }
        }
        scroll.contentSize.height = PADDING_TOP
        
        var y = PADDING_TOP
        for var i = 0; i<items.count; i++ {
            
            let view:UIView = UIView(frame: CGRectMake(0, CGFloat(y), self.view.frame.size.width, 400))
            y += SPAN
            view.backgroundColor = colour(i)
            view.tag = i
            view.layer.cornerRadius = 0//15.0
            view.layer.borderColor = UIColor(white: 230/255, alpha: 1.0).CGColor
            view.layer.borderWidth = 1.0
            
            view.layer.shadowColor = UIColor.clearColor().CGColor//blackColor().CGColor
            view.layer.shadowOpacity = 0.8
            view.layer.shadowRadius = 3.0
            view.layer.shadowOffset = CGSizeMake(0.0, 2.0)
            
            /*var bkgnd:UIImageView = UIImageView(frame: CGRectMake(0, 0, view.frame.size.width, view.frame.size.height))
            bkgnd.contentMode = UIViewContentMode.ScaleAspectFit//Bottom
            bkgnd.image = UIImage(named: "pa")
            bkgnd.alpha = 0.5
            view.addSubview(bkgnd)
            */
            var ico1:UIImageView = UIImageView(frame: CGRectMake(20.0, 20.0, 60.0 , 60.0))
            ico1.contentMode = UIViewContentMode.ScaleAspectFit//Bottom
            var name:NSString = NSString(format: "ico-%i", 1 + Int(arc4random_uniform(UInt32(7))))
            ico1.image = img(name)
            ico1.alpha = 1.0
            view.addSubview(ico1)
            
            var ico2:UIImageView = UIImageView(frame: CGRectMake(100.0, 20.0, 60.0, 60.0))
            ico2.contentMode = UIViewContentMode.ScaleAspectFit//Bottom
            
            var ddd:NSString = NSString(format: "ico-%i", 1 + Int(arc4random_uniform(UInt32(7))))
            while ddd==name {
                ddd = NSString(format: "ico-%i", 1 + Int(arc4random_uniform(UInt32(7))))
            }
            
            ico2.image = img(ddd)
            ico2.alpha = 1.0
            view.addSubview(ico2)
            
            scroll.addSubview(view)
            scroll.contentSize.height += CGFloat(SPAN)
            
            let label:UILabel = UILabel(frame: CGRectMake((view.frame.size.width/2), 20, ((view.frame.size.width/2)-20.0), 60))
            label.numberOfLines = 0
            label.textAlignment = NSTextAlignment.Right
            label.textColor = UIColor.blackColor()
            //label.font = UIFont(name: "Helvetica Neue UltraLight", size: 14.0)
            label.font = UIFont(name: "nevis.ttf", size: 14.0)
            label.text = items[i]["title"] as NSString
            view.addSubview(label)
            
            let textView:UITextView = UITextView(frame: CGRectMake(20, 120, view.frame.width-40, 256))
            textView.textColor = UIColor.blackColor()
            textView.font = UIFont(name: "Helvetica Neue", size: 16)
            textView.backgroundColor = UIColor.clearColor()
            var bulletList:NSMutableString = NSMutableString(capacity: items.count*30)
            for s:NSString in items[i]["text"] as Array {
                bulletList.appendFormat("\u{2022} %@\n", s);
            }
            textView.text = bulletList//items[i]["text"]
            textView.editable = false
            view.addSubview(textView)
            
            let button:UIButton = UIButton(frame: CGRectMake(0, 0, self.view.frame.size.width, 400))
            button.addTarget(self, action: "clicked:", forControlEvents: UIControlEvents.TouchUpInside)
            view.addSubview(button)
            
            let button2:UIButton = UIButton.buttonWithType(UIButtonType.InfoLight) as UIButton
            button2.addTarget(self, action: "turn:", forControlEvents: UIControlEvents.TouchUpInside)
            button2.frame.origin = CGPointMake(view.frame.size.width-44, 356)
            button2.tintColor = UIColor.blackColor()
            view.addSubview(button2)
        }
        
    }
    
    func colour(n:NSInteger) -> UIColor {
        
        if n%2 == 0 {
            return UIColor.whiteColor()
        }
        //return UIColor(white: 245/255, alpha: 1.0)
        return UIColor(red: 243/255, green: 242/255, blue: 237/255, alpha: 1.0)
        
    }
    /*
    var touches_began_location:CGPoint = CGPointMake(0, 0)
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        var touch:UITouch = touches.anyObject() as UITouch
        touches_began_location = touch.locationInView(self.view)
        println("culo")
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        println("aaa")
        var touch:UITouch = touches.anyObject() as UITouch
        var spostamento:CGFloat = touch.locationInView(self.view).y - touches_began_location.y
        touches_began_location = touch.locationInView(self.view)
        for sub in self.scroll.subviews as [UIView] {
            var spost:CGFloat = (spostamento * CGFloat(sub.tag+1) / CGFloat(items.count))
            if sub.frame.origin.y > 60 {
                sub.frame.origin.y += spostamento
            } else if spostamento > 0 {
                sub.frame.origin.y += spostamento
            }
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        println("plof")
    }
    */
    func scrollViewDidScroll(s:UIScrollView) {
        if(s != scroll) {
            return
        }
        let y = s.contentOffset.y
        //println("\(y)")
        for sub in s.subviews as [UIView] {

            if sub.tag == -1 {
                if y < 0 {
                    
                    sub.frame.origin.y = 20.0+(y*0.9)
                    
                } else if y > 140 {
                    sub.frame.origin.y = y-120//PADDING_TOP+(CGFloat(sub.tag*100))
                }
            } else {
        
                if y < 0 {
                    var merda = (PADDING_TOP+(CGFloat(100*sub.tag)))
                
                    sub.frame.origin.y = (merda-((y*CGFloat(sub.tag-6))/7.0))
            
                } else {
                    if y > (CGFloat(sub.tag*100))+140 {
                        sub.frame.origin.y = y+70.0
                    } else {
                        sub.frame.origin.y = PADDING_TOP+(CGFloat(sub.tag*100))
                    }
                }
            }
        }
        
    }
    
    
    @IBAction func clicked(sender:UIButton!) {
        
        let sup:UIView! = sender.superview
        let tag = sup.tag
        println("\(tag)")
        let y = scroll.contentOffset.y

        if !open {
            self.scroll.scrollEnabled = false
            open = true
            
            UIView.animateWithDuration(0.4, animations: {
                var i = 12 + tag //self.items.count
                
                for sub in self.scroll.subviews as [UIView] {
                    if sub.tag != -1 {
                        if sub.tag == tag {
                            sub.frame.origin.y = 80 + self.scroll.contentOffset.y
                            sub.layer.shadowColor = UIColor.blackColor().CGColor
                        } else {
                            sub.frame.origin.y = 45+self.view.frame.size.height -  (10 * CGFloat(i-tag)) + self.scroll.contentOffset.y
                            sub.frame.origin.x += (2 * CGFloat(i-tag))
                            sub.frame.size.width -= (4 * CGFloat(i-tag))
                            i--
                        }
                    }
                }
            })
        } else {
            open = false
            UIView.animateWithDuration(0.4, animations: {
                for sub in self.scroll.subviews as [UIView] {
                    if sub.tag != -1 {

                        /*sub.frame.origin.y = CGFloat(100*sub.tag)
                        if sub.frame.origin.y-20 < self.scroll.contentOffset.y {
                            sub.frame.origin.y = self.scroll.contentOffset.y
                        }
                        sub.frame.size.width = self.view.frame.size.width
                        sub.frame.origin.x = 0
                    
                        sub.layer.shadowColor = UIColor.clearColor().CGColor*/
                        
                        if y < 0 {
                            var merda = (self.PADDING_TOP+(CGFloat(100*sub.tag)))
                            
                            sub.frame.origin.y = (merda-((y*CGFloat(sub.tag-6))/7.0))
                            
                        } else {
                            if y > (CGFloat(sub.tag*100))+140 {
                                sub.frame.origin.y = y+70.0
                            } else {
                                sub.frame.origin.y = self.PADDING_TOP+(CGFloat(sub.tag*100))
                            }
                        }
                        sub.frame.size.width = self.view.frame.size.width
                        sub.frame.origin.x = 0
                        sub.layer.shadowColor = UIColor.clearColor().CGColor
                    }
                }
            })
            self.scroll.scrollEnabled = true
        }
     
    }
    
    @IBAction func turn(sender:UIButton!) {
        println("ahhh")
    }
    
    //MARK: Setup
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return false
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = true
    }
    
    func scan(sender: UIButton!) {
        self.performSegueWithIdentifier("OpenAddView", sender:self)
    }
    
    func img(urlstring :NSString) -> UIImage {
        var url:NSURL = NSURL(string: NSString(format: "http://daviderossetto.it/freego/img/%@.png", urlstring))!
        var request:NSURLRequest = NSURLRequest(URL: url, cachePolicy:NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData , timeoutInterval: 60)
        
        var response: NSURLResponse?
        var error: NSError?
        let urlData = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &error)
        return UIImage(data: urlData!)!
    }
    
}


class PassbookView {
    
    var scroll:UIScrollView = UIScrollView()

    init() {
        
    }
}